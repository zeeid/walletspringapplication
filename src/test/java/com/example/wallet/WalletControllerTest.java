package com.example.wallet;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WalletController.class)
class WalletControllerTest {
    @Autowired
    private MockMvc mockMvc;

    /*
    @Test
    void shouldReturnWalletBalance() throws Exception{
        mockMvc.perform(get("/wallet?userID=5"))
                .andExpect(status().isOk())
                .andExpect(content().string("10.0"));

    }
    */


    @MockBean
    private WalletService walletService;

    @Test
    void testController() throws Exception {
        when(walletService.fetchWallet(1)).thenReturn(new Wallet(1, 10));

        mockMvc.perform(get("/wallet?userId=1"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"userId\":1,\"balance\":10}"));
    }



}
