package com.example.wallet;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WalletServiceTest {
    @Test
    void shouldReturnRightWalletForUser() throws Exception {
        WalletService walletService = new WalletService();
        assertEquals(walletService.fetchWallet(1),(new Wallet(1, 10)));
    }

    @Test
    void shouldReturnRightWalletForAnotherUser() throws Exception {
        WalletService walletService = new WalletService();
        assertEquals(walletService.fetchWallet(3),(new Wallet(3, 30)));
    }
}
