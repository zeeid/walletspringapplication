package com.example.wallet;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Wallet {
    @JsonProperty
    private int userId;
    @JsonProperty
    private double balance;
    public Wallet(int userId, double balance) {
        this.userId = userId;
        this.balance = balance;
    }
    int fetchUserId() {
        return this.userId;
    }

}
