package com.example.wallet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public
class WalletController {
    @Autowired
    private WalletService walletService;

    @GetMapping("/wallet")
    Wallet wallet(@RequestParam("userId") int userId){
        return walletService.fetchWallet(userId);
    }

    @PostMapping("/createWallet")
    Wallet createWallet(@RequestBody Wallet wallet) {
        return walletService.createWallet(wallet);
    }

}
