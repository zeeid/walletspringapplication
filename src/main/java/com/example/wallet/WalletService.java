package com.example.wallet;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public
class WalletService {
    List<Wallet> wallets;

    WalletService() {
        wallets = new ArrayList<>();
        wallets.add(new Wallet(1, 10));
        wallets.add(new Wallet(2, 20));
        wallets.add(new Wallet(3, 30));
    }

    Wallet fetchWallet(int userId) {
        for (Wallet wallet : wallets) {
            if (wallet.fetchUserId()== userId) {
                return wallet;
            }
        }
        return null;
    }

    Wallet createWallet(Wallet wallet) {
        wallets.add(wallet);
        return wallet;
    }
}
